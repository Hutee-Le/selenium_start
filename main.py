from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService

chrome_driver_path = 'Driver/chromedriver-win64/chromedriver.exe'

service = ChromeService(executable_path=chrome_driver_path)
driver = webdriver.Chrome(service=service)


def search_google():
    driver.get("https://www.google.com")
    search_box = driver.find_element("name", "q")
    search_box.send_keys("python selenium")
    search_box.submit()

    driver.implicitly_wait(5)

    search_results = driver.find_elements("css selector", "div.g")
    for index, result in enumerate(search_results[:10], start=1):
        title = result.find_element("css selector", "h3").text
        link = result.find_element("css selector", "a").get_attribute("href")
        print(f"Kết quả #{index}:")
        print(f"Tiêu đề: {title}")
        print(f"Link: {link}")
        print("-" * 50)

    driver.quit()

if __name__ == '__main__':
    search_google()


